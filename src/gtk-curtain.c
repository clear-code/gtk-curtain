#include <gtk/gtk.h>
#include <gdk/gdkwayland.h>

static gboolean fullscreen = FALSE;

static GOptionEntry entries[] =
{
  { "fullscreen", 'f', 0, G_OPTION_ARG_NONE, &fullscreen, "Fullscreen window", NULL },
  { NULL }
};

#if GTK_CHECK_VERSION(3, 24, 22)
static void
cb_window_map(GtkWidget *widget, gpointer data)
{
  gdk_wayland_window_set_application_id(gtk_widget_get_window(widget), data);
}
#endif /* GTK_CHECK_VERSION(3, 24, 22) */

static void
cb_window_destroy(GtkWidget *widget, gpointer data)
{
  gtk_main_quit();
}

int
main(int argc, char *argv[])
{
  GtkWidget *window, *label;
  GError *error = NULL;
  GOptionContext *context;
  const char *app_id = NULL;

  context = g_option_context_new (NULL);
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_add_group (context, gtk_get_option_group (TRUE));
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_warning ("option parsing failed: %s\n", error->message);
    g_error_free(error);
    return 1;
  }

  gdk_set_allowed_backends("wayland");

  gtk_init(&argc, &argv);

  if (argc > 1) {
    app_id = argv[1];
  }

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);
  if (app_id) {
#if GTK_CHECK_VERSION(3, 24, 22)
    g_signal_connect(window, "map",
		     G_CALLBACK(cb_window_map),
		     (gpointer)app_id);
#else
    g_set_prgname(app_id);
#endif
  } else {
    app_id = g_get_prgname();
  }
  g_signal_connect(window, "destroy",
		   G_CALLBACK(cb_window_destroy),
		   NULL);
  if (fullscreen)
    gtk_window_fullscreen(GTK_WINDOW(window));
  label = gtk_label_new(app_id);
  gtk_container_add(GTK_CONTAINER(window), label);
  gtk_widget_show(label);
  gtk_widget_show(window);
  gtk_main();

  g_option_context_free(context);

  return 0;
}
